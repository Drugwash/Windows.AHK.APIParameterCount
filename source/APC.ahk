; � Drugwash, March 2012
#NoEnv
#SingleInstance, Force
ListLines, Off
SetControlDelay, -1
SetWinDelay, -1
SetBatchLines, -1
SetFormat, Float, 1.1
SetFormat, Integer, D
StringCaseSense, Off
DetectHiddenWindows, On
Menu, Tray, UseErrorLevel

appname=API Parameter Count
version=1.0.2.0
releaseD=April 20, 2012
userkey=Software\Drugwash\%appname%
key =%1%

if !A_IsCompiled
	Menu, Tray, Icon, APC.ico
Menu, Tray, Tip, %appname% %version%
Menu, Tray, NoStandard
Menu, Tray, Add, Always on top, AOT
Menu, Tray, Add, Change DB, findDB
Menu, Tray, Add, Save clean DB, saveDB
Menu, Tray, Add
Menu, Tray, Add, About, about
Menu, Tray, Add
Menu, Tray, Add, Reload, reload
Menu, Tray, Add, Exit, GuiClose
RegRead, AOT, HKLM, %userkey%, AlwaysOnTop
if AOT
	Menu, Tray, Check, Always on top
FileInstall, APC_readme.txt, APC_readme.txt, 1
RegRead, file, HKLM, %userkey%, DB
if ErrorLevel
	gosub findDB
FileInstall, APIParameterCount.ini, %file%
Xfile := file
RegRead, section, HKLM, %userkey%, Section
if ErrorLevel
	gosub setSect
if !IniReadEx(file, section, key)
	gosub setSect
if key
	r := IniReadEx(file, section, key, -1)
else goto showList
if (r<0 OR ErrorLevel)
	{
	r1 := IniReadEx(file, section, key "A", -1)
	r2 := IniReadEx(file, section, key "W", -1)
	if (r1<0 && r2<0)
		{
		msgbox, 0x43014, %appname%, Cannot find function %key%().`n`nDo you wanna see the API list?
		IfMsgBox Yes
			goto showList
		ExitApp
		}
	result := (r1 >= "0" ? "� " key "A() requiring " r1 " parameters`n" : "") (r2 >= "0" ? "� " key "W() requiring " r2 " parameters`n" : "")
	msgbox, 0x43030, %appname%, Cannot find function %key%().`n`nHowever, there is:`n%result%
	}
else MsgBox, 0x43040, %appname%, %key%() has %r% parameters.
return
;###############################
GuiSize:
GuiControl, Move, Edit1, % "y" A_GuiHeight-55
GuiControl, Move, Edit2, % "y" A_GuiHeight-55
GuiControl, MoveDraw, Button1, % "y" A_GuiHeight-55
GuiControl, MoveDraw, Button2, % "y" A_GuiHeight-32
GuiControl, MoveDraw, Button3, % "y" A_GuiHeight-32
GuiControl, MoveDraw, Button4, % "y" A_GuiHeight-32
GuiControl, MoveDraw, Button5, % "y" A_GuiHeight-32
GuiControl, Move, LV, % "w" A_GuiWidth-4 " h" A_GuiHeight-59
return
;###############################
reload:
Reload
;###############################
GuiClose:
ExitApp
;###############################
about:
MsgBox, 0x43040, About %appname%, Returns the number of parameters required by Windows APIs`nwhen launched with API name as parameter (i.e. APC __argc),`notherwise presents the list of known* APIs to look through.`n`nThe list should be built as a regular ANSI .ini file (key=value)`nregardless of file size exceeding 64kB (due to special processing).`n`nv%version% � Drugwash, %releaseD%`n`n* The API list may be inaccurate and/or incomplete.`nPlease double-check with MSDN, when possible.
return
;###############################
AOT:
AOT := !AOT
Gui, % (AOT ? "+" : "-") "AlwaysOnTop"
Menu, Tray, ToggleCheck, Always on top
RegWrite, REG_SZ, HKLM, %userkey%, AlwaysOnTop, %AOT%
return
;###############################
findDB:
file := file ? file : A_ScriptDir
FileSelectFile, file, 3, %file%, Select location of API database:, (*.ini)
if (ErrorLevel OR !file)
	goto findDB
RegWrite, REG_SZ, HKLM, %userkey%, DB, %file%
setSect:
InputBox, section, Select section, Type in the section name to read from:,, 250, 120,,,,, APIParameterCount
if !section
	goto setSect
RegWrite, REG_SZ, HKLM, %userkey%, Section, %section%
if !vis
	return
Gui, Destroy
;###############################
showList:
FileRead,txt, %file%
StringReplace, txt, txt,`r`n, `n, All
StringReplace, txt, txt,`n`n, `n, All
if !InStr(txt, "[" section "]`n")
	return
StringReplace, txt, txt, [%section%]`n,,
Sort, txt, U
line=0
i=0
Loop
	{
	if !i := InStr(txt, "=", false, i+1)
		break
	line++
	}
Progress, B1 M W160 ZH12 FM9 FS8 WM700 R1-100 CBRed CTWhite CWPurple, please wait..., Loading %line% APIs,,Tahoma
Gui, % "+Resize +MinSize430x310 " (AOT ? "+" : "-") "AlwaysOnTop"
Gui, Margin, 2, 2
Gui, Add, ListView, w500 h300 AltSubmit Count%line% ReadOnly Sort -NoSortHdr -Multi Grid -LV0x10 hwndhLV vLV gselect, Function name|Params|Library|�
LV_ModifyCol(2, "Center")
Gui, Add, Edit,w500 h21 vin gsearch, %key%
Gui, Add, Edit, x+3 yp w40 h21 Center Number Limit3 vinp,
Gui, Add, Button, x+1 yp w60 h21 gmodify, Add/Edit
Gui, Add, Button, xm yp w40 h30 gGuiClose, Exit
Gui, Add, Button, x+5 w70 h30 gcopy, Copy name
Gui, Add, Button, x+5 w70 h30 gcopyC, Copy value
Gui, Add, Button, x+5 yp w90 h30 Disabled gsaveMod, Save file
GuiControl, -Redraw, SysListView321
Loop, Parse, txt, `n, `r
	{
	Progress, % (A_Index*100)//line
	i=%A_LoopField%
	if (!i OR InStr(i, ";")=1)
		continue
	StringSplit, i, i, =
	i1=%i1%
	i2=%i2%
	LV_Add("", i1, i2)
	}
LV_ModifyCol(1, "AutoHdr")
LV_ModifyCol(2, "AutoHdr Logical")
LV_ModifyCol(3, 0)
LV_ModifyCol(4, 0)
t := LV_GetCount()
GuiControl, +Redraw, SysListView321
Progress, Off
Gui, Show, w430 h310, %appname% [%t% APIs]
vis=1
OnMessage(0x4E, "notify")	; WM_NOTIFY
LV_ModifyCol(4, 0)
return
;###############################
copy:
row := LV_GetNext()
LV_GetText(r, row)
Clipboard := r
return
;###############################
copyC:
row := LV_GetNext()
LV_GetText(r, row, 2)
Clipboard := r
return
;###############################
select:
row := A_EventInfo
if A_GuiEvent in DoubleClick
	goto copy
if A_GuiEvent in Normal
	{
	LV_GetText(r, row)
	set=1
	GuiControl,, Edit1, %r%
	LV_GetText(r, row, 2)
	GuiControl,, Edit2, %r%
	}
return
;###############################
search:
Gui, Submit, NoHide
if set
	{
	set=
	return
	}
VarSetCapacity(LVFI, 24, 0)
NumPut(0xA, LVFI, 0, "UInt")	; LVFI_STRING LVFI_PARTIAL
NumPut(&in, LVFI, 4, "UInt")	; psz
ro := DllCall("SendMessage", "UInt", hLV, "UInt", 0x100D, "UInt", -1, "UInt", &LVFI)+1	; LVM_FINDITEM
if ro=0
	{
	set=1
	LV_Modify(1, "Select Vis")
	return
	}
DllCall("SendMessage", "UInt", hLV, "UInt", 0x1013, "UInt", ro-1, "UInt", 0)	; LVM_ENSUREVISIBLE
VarSetCapacity(LVI, 40, 0)
NumPut(0x8, LVI, 0, "UInt")	; LVIF_STATE
NumPut(ro-1, LVI, 4, "Int")	; iItem
NumPut(0x2, LVI, 12, "UInt")	; state (LVIS_SELECTED=2 LVIS_FOCUSED=1)
NumPut(0x2, LVI, 16, "UInt")	; stateMask
DllCall("SendMessage", "UInt", hLV, "UInt", 0x102B, "UInt", ro-1, "UInt", &LVI)	; LVM_SETITEMSTATE
return
;###############################
modify:
Gui, Submit, NoHide
IniWriteEx(inp, file, section, in, 0)
StringReplace, txt, file, [%section%]`n,,
Sort, txt, U
file=[%section%]`n%txt%
Loop, % LV_GetCount()
	{
	LV_GetText(i, A_Index)
	if (i=in)
		{
		LV_Modify(A_Index, "Col2", inp)
		return
		}
	}
LV_Modify(LV_Add("", in, inp), "Vis Select Focus")
t := LV_GetCount()
Gui, Show,, %appname% [%t% APIs]
GuiControl, Enable, Button5
return
;###############################
saveDB:
FileSelectFile, Xfile, 18, %Xfile%, Save cleaned-up DB as:, INI file (*.ini)
if (!Xfile OR ErrorLevel)
	return
saveMod:
GuiControl, Disable, Button5
SplitPath, Xfile,, Xdir,, Xname
Xfile := Xdir "\" Xname ".ini"
FileDelete, %Xfile%
FileAppend, [%section%]`n%txt%, *%Xfile%
return
;###############################
notify(wP, lP, msg, hwnd)
{
ofi := A_FormatInteger
SetFormat, Integer, D
hwndFrom := NumGet(lP+0, 0, "UInt")
code := NumGet(lP+0, 8, "Int")
cmdid := NumGet(lP+0, 12, "UInt")
if code in -307,-327,-301,-321,-300,-320	; HDN_ENDTRACK HDN_ITEMCHANGED HDN_ITEMCHANGING
	{
	hList := DllCall("GetParent", "UInt", hwndFrom)
	VarSetCapacity(RECT, 16, 0)
	DllCall("SendMessage", "UInt", hwndFrom, "UInt", 0x1207, "UInt", 0, "UInt", &RECT)	; HDM_GETITEMRECT
	w := NumGet(RECT, 8, "Int") - NumGet(RECT, 0, "Int")+2
	GuiControl, Move, Edit1, w%w%
	DllCall("SendMessage", "UInt", hwndFrom, "UInt", 0x1207, "UInt", 1, "UInt", &RECT)	; HDM_GETITEMRECT
	x := NumGet(RECT, 0, "Int")+5
	w := NumGet(RECT, 8, "Int") - NumGet(RECT, 0, "Int")
	GuiControl, Move, Edit2, x%x% w%w%
	GuiControl, Move, Button1, % "x" x+w+2
	}
SetFormat, Integer, %ofi%
}

#include func_IniEx.ahk
